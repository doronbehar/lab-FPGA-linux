{
  description = "Molecular Duck";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nix-xilinx = {
      url = "gitlab:doronbehar/nix-xilinx";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self
  , nixpkgs
  , nix-xilinx
  }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
        nix-xilinx.overlay
      ];
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      nativeBuildInputs = [
        # Requires an imperative installation, as explained in nix-xilinx README:
        # https://gitlab.com/doronbehar/nix-xilinx
        pkgs.petalinux
        # Add tools for reverse engineering prebuilt images
        pkgs.ubootTools
        pkgs.pax
        pkgs.binwalk
        pkgs.lhasa
      ];
    };
    packages.x86_64-linux = {
      # From the overlays
      inherit (pkgs)
        petalinux
      ;
    };
  };
}
